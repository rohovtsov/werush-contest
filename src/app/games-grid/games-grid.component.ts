import {Component, HostBinding, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {Game} from '../api/game';

@Component({
  selector: 'app-games-grid',
  templateUrl: './games-grid.component.html',
  styleUrls: ['./games-grid.component.scss']
})
export class GamesGridComponent implements OnInit, OnChanges {
  @HostBinding('class.with-animation') withAnimation = false;
  @HostBinding('class.is-visible') isVisible = true;
  @Input() games: Game[] = [];
  animationTimeout: any = null;

  constructor() { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if ('games' in changes && !changes.games.firstChange) {
      this.withAnimation = false;
      this.isVisible = false;
      clearTimeout(this.animationTimeout);
      this.animationTimeout = setTimeout(() => {
        this.withAnimation = true;
        this.isVisible = true;
      }, 240);
    }
  }

  trackByGame(index: number, game: Game): number {
    return game.id;
  }
}
