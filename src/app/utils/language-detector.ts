import {LanguageAbbr} from '../api/language';



const langAbbrs = Object.keys(LanguageAbbr)
  .filter(k => typeof LanguageAbbr[k] === 'string').map(k => LanguageAbbr[k]);


export const getNavigatorLangAbbr = (): LanguageAbbr => {
  const languages = [navigator.language, ...navigator.languages];
  let abbr = null;

  languages.forEach((language) => {
    if (abbr != null || !language) {
      return;
    }

    const foundAbbr = langAbbrs.find((itemAbbr) => {
      return language.trim().toLowerCase().includes(itemAbbr);
    });

    if (foundAbbr != null) {
      abbr = foundAbbr;
    }
  });

  return abbr;
};


export const findLangAbbrOrUseDefault = (langAbbr: string): LanguageAbbr => {
  const parsedLangAbbr = (langAbbr ?? '').trim().toLowerCase();

  if (langAbbrs.includes(parsedLangAbbr)) {
    return parsedLangAbbr as LanguageAbbr;
  }

  return getNavigatorLangAbbr() ?? LanguageAbbr.ENGLISH;
};
