import {Game} from '../api/game';
import {Category} from '../api/category';

export class Controller {
  public displayedGames: Game[];
  public activeCategory: Category;
  public searchQuery: string;


  constructor(
    private categories: Category[],
    private games: Game[]
  ) {
    this.displayedGames = games;
  }

  onActiveCategoryChange(category: Category): void {
    this.activeCategory = category;
    this.updateDisplayedGames();
  }

  onSearchQueryChange(query: string): void {
    this.searchQuery = query;
    this.updateDisplayedGames();
  }

  private updateDisplayedGames(): void {
    this.displayedGames = this.filterGames(this.games, this.searchQuery, this.activeCategory);
  }

  private filterGames(games: Game[], query: string, category: Category): Game[] {
    const parsedQuery = (query ?? '').toLowerCase().trim();
    let filteredGames = [...games];

    if (parsedQuery) {
      filteredGames = filteredGames.filter((game) => {
        return game.title.toLowerCase().trim().includes(parsedQuery);
      });
    }

    if (category) {
      filteredGames = filteredGames.filter((game) => {
        return game.categories.includes(category.id);
      });
    }

    return filteredGames;
  }
}
