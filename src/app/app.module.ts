import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PageHomeComponent } from './page-home/page-home.component';
import { HttpClientModule } from '@angular/common/http';
import { CategoriesSelectComponent } from './categories-select/categories-select.component';
import { SearchInputComponent } from './search-input/search-input.component';
import { GamesGridComponent } from './games-grid/games-grid.component';
import { FooterComponent } from './footer/footer.component';
import { LanguageSelectComponent } from './language-select/language-select.component';

@NgModule({
  declarations: [
    AppComponent,
    PageHomeComponent,
    CategoriesSelectComponent,
    SearchInputComponent,
    GamesGridComponent,
    FooterComponent,
    LanguageSelectComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
