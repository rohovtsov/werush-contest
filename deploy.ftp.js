const FtpDeploy = require("ftp-deploy");
const GLOBAL_CONFIG = require("./deploy.ftp.conf");


/**
 * GLOBAL_CONFIG EXAMPLE
 */
module.exports = [
  {
    branch: '*|master|other_branch',
    config: [
      {
        user: "USER",
        password: "PASSWORD",
        host: "HOST",
        localRoot: __dirname + "/dist/project_name/",
        remoteRoot: "/public_html/remote-folder/dist/",
        excludeAlso: ['assets/**'],
        deleteRemote: true
      },
      {
        user: "USER",
        password: "PASSWORD",
        host: "HOST",
        localRoot: __dirname + "/dist/project_name/assets/",
        remoteRoot: "/public_html/remote-folder/assets/"
      }
    ]
  }
];



const getGitBranchName = () => {
  const { exec } = require('child_process');

  return new Promise((resolve, reject) => {
    exec('git rev-parse --abbrev-ref HEAD', (err, stdout, stderr) => {
      if (err) {
        reject(err);
      }

      if (typeof stdout === 'string') {
        const name = stdout.trim();
        resolve(name);
      }
    });
  })
};


const createFullConfig = (userConfig) => {
  const defaultConfig = {
    port: 21,
    include: ["*"],
    exclude: ["**/*.map", "node_modules/**", "node_modules/**/.*", ".git/**"],
    deleteRemote: false,
    forcePasv: true,
    sftp: false
  };

  if (userConfig.excludeAlso && userConfig.excludeAlso.length) {
    defaultConfig.exclude = [
      ...defaultConfig.exclude,
      ...userConfig.excludeAlso
    ]
  }

  return {
    ...defaultConfig,
    ...userConfig
  };
};


const deployConfig = (config) => {
  const ftpDeploy = new FtpDeploy();

  ftpDeploy.on("uploaded", function(data) {
    console.log('Uploaded: ' + data.transferredFileCount + '/' + data.totalFilesCount + ' ' + data.filename);
  });

  ftpDeploy.on("log", function(data) {
    console.log(data); // same data as uploading event
  });

  ftpDeploy.on("upload-error", function(data) {
    console.log('Error: ' + data.transferredFileCount + '/' + data.totalFilesCount + ' ' + data.filename);
  });

  return ftpDeploy.deploy(config);
};


const printDeployEnd = (config, branchName, time) => {
  console.log('Finished deploy of `' + branchName + '` in `' + time + 'ms`');
};


const deployGlobalConfigSet = async (configSet) => {
  const branchName = await getGitBranchName();

  const foundConfig = configSet.find((config) => {
    return ['*', branchName].includes(config.branch) && !!config.config;
  });

  if (!foundConfig || !foundConfig.config) {
    throw Error('Git branch `' + branchName + '` doesn\'t correspond to any config');
  }

  const configsArray = foundConfig.config.length ? foundConfig.config : [foundConfig.config];

  let totalTime = 0;

  for (const configItem of configsArray) {
    const config = createFullConfig(configItem);

    const startTime = Date.now();
    await deployConfig(config);
    const pastTime = Date.now() - startTime;
    totalTime += pastTime;

    printDeployEnd(config, branchName, pastTime);
  }

  console.log('Total time `' + totalTime + 'ms`');
};



deployGlobalConfigSet(GLOBAL_CONFIG)
  .catch(console.error);
