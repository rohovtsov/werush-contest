import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router} from '@angular/router';
import {Observable} from 'rxjs';
import {findLangAbbrOrUseDefault} from './utils/language-detector';

@Injectable({
  providedIn: 'root'
})
export class LanguageGuard implements CanActivate {
  constructor(
    private router: Router
  ) { }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    const urlLang = route.params.lang;
    const lang = findLangAbbrOrUseDefault(urlLang);

    if (urlLang !== lang) {
      return this.router.parseUrl('/' + lang);
    }

    return true;
  }
}
