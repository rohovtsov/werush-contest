import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Category} from '../api/category';



@Component({
  selector: 'app-categories-select',
  templateUrl: './categories-select.component.html',
  styleUrls: ['./categories-select.component.scss']
})
export class CategoriesSelectComponent implements OnInit {
  @Input() categories: Category[] = [];
  @Output() categoryChange = new EventEmitter<Category>();
  cards: Card[];
  selectedCard: Card;

  constructor() { }

  ngOnInit(): void {
    this.cards = this.categories.map((cat) => ({
      title: cat.name,
      description: cat.description,
      category: cat
    }));
  }

  onCardClick(card: Card): void {
    if (this.selectedCard === card) {
      this.selectCard(null);
      return;
    }

    this.selectCard(card);
  }

  private selectCard(card: Card): void {
    this.selectedCard = card;
    this.categoryChange.emit(card ? card.category : null);
  }
}


interface Card {
  title: string;
  description: string;
  category: Category;
}
