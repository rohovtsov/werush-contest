import {Component, Input, OnInit} from '@angular/core';
import {Language} from '../api/language';
import {FooterList, parseLists} from './footer-list';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  @Input() language: Language;
  lists: FooterList[];

  constructor() { }

  ngOnInit(): void {
    this.lists = parseLists(this.language);
  }

}
