import { Injectable } from '@angular/core';
import {
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable } from 'rxjs';
import {Language} from './api/language';
import {ApiService} from './api.service';

@Injectable({
  providedIn: 'root'
})
export class LanguageResolver implements Resolve<Language> {
  constructor(
    private apiService: ApiService
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Language> {
    const lang = route.params.lang;
    return this.apiService.getLanguage(lang);
  }
}
