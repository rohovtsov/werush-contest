export enum LanguageAbbr {
  ENGLISH = 'en', RUSSIAN = 'ru'
}

export interface Language {
  abbr: LanguageAbbr;
  data: Record<string, string>;
}
