import {Language} from '../api/language';



export interface FooterList {
  title: string;
  items: string[];
}


export const parseLists = (language: Language): FooterList[] => {
  const data: any = language.data;
  const dataKeys = Object.keys(data);
  const listMap: Record<string, FooterList> = {};

  for (const key of dataKeys) {
    if (key.includes('footerListTitle')) {
      listMap[key.replace('footerListTitle', '')] = {
        title: data[key],
        items: []
      };
    }
  }

  for (const key of dataKeys) {
    if (key.includes('footerListItem')) {
      const pair = key.replace('footerListItem', '').split('.').map(item => Number(item));
      listMap[String(pair[0])].items[pair[1] - 1] = data[key];
    }
  }

  return Object.keys(listMap).map(key => listMap[key]);
};
