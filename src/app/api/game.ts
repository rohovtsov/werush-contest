export interface Game {
  id: number;
  title: string;
  categories: number[];
  image: string;
}
