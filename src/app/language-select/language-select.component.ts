import {Component, HostBinding, HostListener, Input, OnInit, ViewChild} from '@angular/core';
import {Language, LanguageAbbr} from '../api/language';
import {Router} from '@angular/router';

@Component({
  selector: 'app-language-select',
  templateUrl: './language-select.component.html',
  styleUrls: ['./language-select.component.scss']
})
export class LanguageSelectComponent implements OnInit {
  private items: LangItem[] = [
    new LangItem(LanguageAbbr.ENGLISH, 'langEn'),
    new LangItem(LanguageAbbr.RUSSIAN, 'langRu')
  ];
  itemOptions: LangItem[];
  selectedItem: LangItem;
  @HostBinding('class.is-expanded') isExpanded = false;
  @HostBinding('class.is-inverted-popup') invertedPopup = false;
  @Input() language: Language;

  constructor(
    private router: Router
  ) { }

  ngOnInit(): void {
    this.items.forEach(item => {
      item.setLabel(this.language);
    });
    this.selectItemByAbbr(this.language ? this.language.abbr : LanguageAbbr.ENGLISH);
  }

  private selectItemByAbbr(abbr: string): void {
    const selectedItem = this.items.find((item) => {
      return item.abbr === abbr;
    });

    if (selectedItem) {
      this.selectItem(selectedItem);
    }
  }

  private selectItem(item: LangItem): void {
    this.selectedItem = item;
    this.itemOptions = this.items
      .filter((langItem) => langItem.abbr !== item.abbr);
  }

  private toggleExpanded(isExpanded): void {
    this.isExpanded = isExpanded;

    if (isExpanded) {
      this.selectItem(this.selectedItem);
    }
  }

  @HostListener('window:click', ['$event'])
  onOutsideClick(event): void {
    const closest = event.target.closest('app-language-select');

    if (!closest) {
      this.toggleExpanded(false);
    }
  }

  onSwitchClick(): void {
    this.toggleExpanded(!this.isExpanded);
  }

  onItemClick(item: LangItem): void {
    if (this.selectedItem === item) {
      return;
    }

    this.selectedItem = item;
    this.toggleExpanded(false);
    this.router.navigate(['/' + item.abbr]);
  }
}


class LangItem {
  public label: string;

  constructor(
    public abbr: LanguageAbbr,
    public labelSlug: string
  ) {}

  setLabel(language: Language): void {
    this.label = language.data[this.labelSlug];
  }
}
