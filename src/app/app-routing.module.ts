import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageHomeComponent } from './page-home/page-home.component';
import {LanguageGuard} from './language.guard';
import {LanguageResolver} from './language.resolver';

const routes: Routes = [
  { path: ':lang', canActivate: [LanguageGuard], resolve: { language: LanguageResolver }, component: PageHomeComponent },
  { path: '', canActivate: [LanguageGuard], component: PageHomeComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
