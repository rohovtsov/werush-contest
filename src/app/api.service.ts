import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Language, LanguageAbbr} from './api/language';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {Category} from './api/category';
import {Game} from './api/game';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(
    private http: HttpClient
  ) { }

  getLanguage(abbr: LanguageAbbr): Observable<Language> {
    return this.http.get('/assets/lang/' + abbr + '.json').pipe(
      map((data) => ({ abbr, data: (data as Record<string, string>)}))
    );
  }

  getCategories(abbr: LanguageAbbr): Observable<Category[]> {
    return this.http.get('/assets/' + abbr + '/categories.json') as Observable<Category[]>;
  }

  getGames(abbr: LanguageAbbr): Observable<Game[]> {
    return this.http.get('/assets/' + abbr + '/games.json') as Observable<Game[]>;
  }
}
