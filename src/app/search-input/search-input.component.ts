import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-search-input',
  templateUrl: './search-input.component.html',
  styleUrls: ['./search-input.component.scss']
})
export class SearchInputComponent implements OnInit {
  @Input() placeholder: string;
  @Output() queryChange = new EventEmitter<string>();
  private query = '';

  constructor() { }

  ngOnInit(): void {
  }

  onInputChange(event: Event): void {
    const value = (event.target as HTMLInputElement).value;
    this.setQuery(value);
  }

  private setQuery(value: string): void {
    this.query = value;
    this.queryChange.emit(value);
  }
}
