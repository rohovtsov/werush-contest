import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {BehaviorSubject, zip} from 'rxjs';
import {Language} from '../api/language';
import {ApiService} from '../api.service';
import {Game} from '../api/game';
import {Category} from '../api/category';
import {Controller} from './controller';

@Component({
  selector: 'app-page-home',
  templateUrl: './page-home.component.html',
  styleUrls: ['./page-home.component.scss']
})
export class PageHomeComponent implements OnInit {
  language: Language;
  games: Game[];
  categories: Category[];
  controller: Controller;

  constructor(
    private activatedRoute: ActivatedRoute,
    private apiService: ApiService
  ) { }

  ngOnInit(): void {
    this.language = (this.activatedRoute.data as BehaviorSubject<any>).value.language;

    zip(
      this.apiService.getCategories(this.language.abbr),
      this.apiService.getGames(this.language.abbr),
    ).subscribe(([categories, games]) => {
      this.categories = categories;
      this.games = games;
      this.controller = new Controller(this.categories, this.games);
    });
  }

}
